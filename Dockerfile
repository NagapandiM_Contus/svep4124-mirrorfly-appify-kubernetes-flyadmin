

# Start with a base image containing Java runtime
FROM openjdk:8-jdk-alpine

RUN mkdir -pv /opt/files

# Add a volume pointing to /tmp
VOLUME /tmp


COPY config /opt/files




# Add a volume pointing to /tmp
VOLUME /tmp


# The application's jar file
ARG JAR_FILE=target/flyadmin.jar

# Add the application's jar to the container
ADD ${JAR_FILE} flyadmin.jar

# Run the jar file 
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/flyadmin.jar"]
