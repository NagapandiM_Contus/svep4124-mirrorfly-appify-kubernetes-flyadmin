package com.mirrorfly.flyadmin.service;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mirrorfly.flyadmin.config.ApplicationProperties;
import com.mirrorfly.flyadmin.dto.StatusRequestDto;
import com.mirrorfly.flyadmin.dto.StatusResponseDto;
import com.mirrorfly.flyadmin.util.Constants;

import io.kubernetes.client.ApiClient;
import io.kubernetes.client.ApiException;
import io.kubernetes.client.Configuration;
import io.kubernetes.client.apis.CoreV1Api;
import io.kubernetes.client.models.V1Pod;
import io.kubernetes.client.models.V1PodList;
import io.kubernetes.client.util.ClientBuilder;
import io.kubernetes.client.util.KubeConfig;
import io.kubernetes.client.util.authenticators.GCPAuthenticator;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class StatusService {

	private ApiClient apiClient = null;

	private List<String> clusters = new ArrayList<>();

	private Map<String, String> clusterMap = new HashMap<>();

	@Autowired
	private ApplicationProperties applicationProperties;

	public void checkServerStatus() {
		try {
			getAPIClient();
			CoreV1Api api = new CoreV1Api();
			V1PodList pods = api.listNamespacedPod(applicationProperties.getNameSpace(), null, null, null, null,
					"run=chatservice", null, null, null, null);
			if (pods != null && pods.getItems() != null && !pods.getItems().isEmpty()) {
				List<String> clusterList = new ArrayList<>();
				clusterMap = new HashMap<>();
				for (V1Pod pod : pods.getItems()) {
					if (pod.getStatus().getPhase().equals("Running") && pod.getStatus().getPodIP() != null) {
						log.info("Pod Name {} IP {}", pod.getMetadata().getName(), pod.getStatus().getPodIP());
						clusterList.add(pod.getStatus().getPodIP());
						clusterMap.put(pod.getStatus().getPodIP(), pod.getMetadata().getName());
					}
				}
				if (clusterList != null && !clusterList.isEmpty())
					getEjabberdClusterStatus(clusterList);
			} else {
				log.info("No Pods Available");
			}

		} catch (IOException e) {
			log.error("IoException in checkServerStatus", e);
		} catch (ApiException e) {
			log.error("ApiException in  checkServerStatus", e);
		}
	}

	private void getEjabberdClusterStatus(List<String> clusterList) {
		checkClusterStatus(clusterList);
		log.info("Master Server IP Address {}", clusterList.get(0));
		for (String ipAddress : clusterList) {
			addToCluster(ipAddress, clusterList.get(0));
		}
	}

	private void checkClusterStatus(List<String> clusterList) {
		StatusResponseDto response;
		String node;
		for (String ipAddress : clusterList) {
			try {
				node = Constants.NODE_NAME + ipAddress;
				log.info("Checking Cluster Status for Server {} {}", ipAddress, node);
				response = restCall(Constants.EXEC + node + Constants.LIST_CLUSTER, ipAddress);
				if (response.getMessage().contains("Failed RPC connection to the node")) {
					startEjabberdServer(ipAddress);
				}
			} catch (Exception ex) {
				log.error("Exception in Ejabberd Cluster status {}", ex);
			}
		}
	}

	public void addToCluster(String ipAddress, String masterIP) {
		if (!ipAddress.equals(masterIP)) {
			String node = Constants.NODE_NAME + ipAddress;
			StatusResponseDto response = restCall(Constants.EXEC + node + Constants.LIST_CLUSTER, ipAddress);
			try {
				if (!response.getMessage().contains(masterIP)) {
					log.info("{} Not in Cluster,Adding to Cluster  ", ipAddress);
					response = restCall(Constants.EXEC + node + Constants.JOIN_CLUSTER + masterIP, ipAddress);
					log.info("Cluster Join Command Response {} ", response.getMessage());
					clusters.add(clusterMap.get(ipAddress));
					Thread.sleep(5000l);
				} else {
					log.info("{} Already added to Cluster {} ", ipAddress, response.getMessage());
				}
			} catch (Exception ex) {
				log.error("Exception in Ejabberd {}", ex);
			}
		}
	}

	private void startEjabberdServer(String ipAddress) {
		String node;
		try {
			node = Constants.NODE_NAME + ipAddress;
			StatusResponseDto response;
			log.info("{} -->  Starting Ejabberd Server {}", ipAddress, node);
			response = restCall(Constants.EXEC + node + Constants.START_COMMAND, ipAddress);
			log.info("{} --> Waiting {} seconds after executing Ejabberd Start command ", ipAddress, "5");
			Thread.sleep(5000);
			response = restCall(Constants.EXEC + node + Constants.STATUS, ipAddress);
			if (response.getMessage().contains("Failed RPC connection to the node")) {
				log.info("{} --> Ejabberd Not Started in Server {} ", ipAddress, response.getMessage());
				log.info("{} --> Send Alert that Ejabberd Not Starting in Pod", ipAddress);
			}
			if (response.getMessage().contains("started")) {
				log.info("{} --> Ejabberd Running {}", ipAddress, response.getMessage());
			}
		} catch (Exception ex) {
			log.error("Server {} Not  Starting ", ipAddress);
			log.error("Exception in Ejabberd {}", ex);
		}
	}

	private ApiClient getAPIClient() throws IOException {

		if (apiClient == null) {
			log.info("Configuring Kubernetes API Client");
			log.info("config {}", applicationProperties.getAuthenticationType());
			if (applicationProperties.getAuthenticationType() != null
					&& applicationProperties.getAuthenticationType().contains("config")) {
				log.info("From config {}", applicationProperties.getAuthenticationType());
				// loading the out-of-cluster config, a kubeconfig from file-system
				apiClient = ClientBuilder.kubeconfig(
						KubeConfig.loadKubeConfig(new FileReader(applicationProperties.getAuthenticationType())))
						.build();
			} else {
				log.info("From Cluster binding");
				apiClient = ClientBuilder.cluster().build();
				KubeConfig.registerAuthenticator(new GCPAuthenticator());

			}
			// set the global default api-client to the in-cluster one from above
			Configuration.setDefaultApiClient(apiClient);
		}
		return apiClient;
	}

	public StatusResponseDto restCall(String command, String ipAddress) {
		RestTemplate restTemplate = new RestTemplate();
		log.info("{} --> {}", ipAddress, command);
		HttpEntity<StatusRequestDto> request = new HttpEntity<>(new StatusRequestDto(command));
		ResponseEntity<StatusResponseDto> response = restTemplate.exchange("http://" + ipAddress + Constants.API,
				HttpMethod.POST, request, StatusResponseDto.class);
		return response.getBody();

	}
}
