package com.mirrorfly.flyadmin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class StatusSchedular {

	@Autowired
	private StatusService statusService;

	@Scheduled(fixedDelay = 60000, initialDelay = 20000)
	public void checkServerStatus() {
		log.info("schedular started");
		statusService.checkServerStatus();
		log.info("schedular completed");
	}

}
