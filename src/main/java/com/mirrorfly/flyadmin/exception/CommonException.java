package com.mirrorfly.flyadmin.exception;

/**
 *
 */
public class CommonException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4108970394919771532L;

	public CommonException(String message) {
		super(message);
	}

}
