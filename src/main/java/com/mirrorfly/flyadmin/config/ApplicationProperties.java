package com.mirrorfly.flyadmin.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class ApplicationProperties {

	@Autowired
	private Environment env;

	public String getAuthenticationType() {
		return env.getProperty("auth.type");
	}

	public String getNameSpace() {
		return env.getProperty("namespace");
		}
	
}
