package com.mirrorfly.flyadmin.util;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.List;

public class ChatUtil {

	private ChatUtil() {

	}

	public static LocalDateTime getCurrentDateTime() {
		return LocalDateTime.now();
	}

	public static String getName(String firstName, String lastName) {
		return firstName + " " + lastName;
	}

	public static LocalDateTime getLocaDateTime(Long timestamp) {
		Instant instant = Instant.ofEpochMilli(timestamp / 1000);
		LocalDateTime date = instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
		return date;
	}

	public static List<String> getCommaSeperatedString(String text) {
		return text == null ? null : Arrays.asList(text.split("\\s*,\\s*"));
	}

	public static String getCommaSeperatedString(List<String> text) {
		return String.join(",", text);
	}

	public static String splitUserNameFromJid(String jid) {
		return jid.split("@", 2)[0];
	}
}
