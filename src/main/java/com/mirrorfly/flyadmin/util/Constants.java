package com.mirrorfly.flyadmin.util;

public class Constants {

	private Constants() {

	}

	public static final String API = ":9090/ejabberd/ctl";

	public static final String EXEC = "/opt/ejabberd/sbin/ejabberdctl ";

	public static final String STATUS = " status ";

	public static final String START_COMMAND = " start ";

	public static final String NODE_NAME = " --node ejabberd@";

	public static final String EJABBERDUSR = " --node ejabberd@";

	public static final String JOIN_CLUSTER = " --no-timeout join_cluster ejabberd@";

	public static final String LIST_CLUSTER = " list_cluster ";

}
