package com.mirrorfly.flyadmin.dto;

import lombok.Data;

@Data
public class StatusResponseDto {

	private String message;

	private String status;

	public StatusResponseDto(String status) {
		super();
		this.status = status;
	}

	public StatusResponseDto() {
		super();
		// TODO Auto-generated constructor stub
	}

}
