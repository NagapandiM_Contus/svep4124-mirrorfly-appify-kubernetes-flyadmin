package com.mirrorfly.flyadmin.dto;

import lombok.Data;

@Data
public class StatusRequestDto {

	private String status;

	public StatusRequestDto(String status) {
		super();
		this.status = status;
	}

	public StatusRequestDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
